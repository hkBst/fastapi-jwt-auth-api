import pytest

from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)

@pytest.fixture
def user():
    return {'username':'MS', 'firstname':'M', 'lastname':'S', 'password':'a1'}

def test_create_user_missing_password():
    response = client.post("/users", json = {'username':'MS', 'firstname':'M', 'lastname':'S'})
    assert response.status_code == 422 # 422 Unprocessable Entity

def test_create_user_missing_username():
    response = client.post("/users", json = {'firstname':'M', 'lastname':'S', 'password':'a1'})
    assert response.status_code == 422 # 422 Unprocessable Entity

def test_create_user(user):
    response = client.post("/users", json = user)
    assert response.status_code == 200
    assert response.json() == {"msg": "user created"}

def test_create_duplicate_user(user):
    response = client.post("/users", json = user)
    assert response.status_code == 200
    assert response.json() == {"msg": "user already exists"}

def test_login_wrong_password():
    response = client.post("/login", data = {'username':'MS', 'password':'b2'})
    assert response.status_code == 401
    assert response.json() == {'detail': 'invalid login credentials'}

def test_login_nonexistent_user():
    response = client.post('/login', data = {'username':'nobody', 'password':'nothing'})
    assert response.status_code == 401
    assert response.json() == {'detail': 'invalid login credentials'}

def test_login():
    response = client.post('/login', data = {'username':'MS', 'password':'a1'})
    assert response.status_code == 200
    response_json = response.json()
    assert 'access_token' in response_json
    assert response_json['token_type'] == 'bearer'

def get_token(username, password):
    response = client.post('/login', data = {'username':username, 'password':password})
    return response.json()['access_token']

def test_update_password():
    response = client.put('/users',
                          json = {'firstname':'Mar', 'lastname':'Sch', 'password':'b2'},
                          headers = {'Authorization': f"Bearer {get_token('MS', 'a1')}"}
                          )
    assert response.status_code == 200

def test_login_with_old_password():
    response = client.post('/login', data = {'username':'MS', 'password':'a1'})
    assert response.status_code == 401

def test_login_with_new_password():
    response = client.post('/login', data = {'username':'MS', 'password':'b2'})
    assert response.status_code == 200

def test_delete_user():
    response = client.delete('/users',
                             headers = {'Authorization': f"Bearer {get_token('MS', 'b2')}"}
                             )
    assert response.status_code == 200

def test_login_with_deleted_user():
    response = client.post('/login', data = {'username':'MS', 'password':'b2'})
    assert response.status_code == 401

import datetime
import hashlib
import os

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

from pydantic import BaseModel
from jose import jwt

class User(BaseModel):
    username: str
    firstname: str
    lastname: str
    password: str

class UserUpdate(BaseModel):
    firstname: str
    lastname: str
    password: str

class Token(BaseModel):
    access_token: str
    token_type: str

N_HASH_ITERATIONS = 1_000_000
N_SALT_RANDOM_BYTES = 32

# FIXME: store somewhere safe
SECRET = '58e8d846ad5e20b1c6c709974e3e3b87ccc8b7dbbf9b539f89889d04715fed22'

DB = {} # FIXME: use a proper database

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='login')

login_exception = HTTPException(
    status_code = status.HTTP_401_UNAUTHORIZED,
    detail = 'invalid login credentials',
    headers = {'WWW-Authenticate': 'Bearer'},
)

### CREATE (API) ###
@app.post('/users')
async def create_api(user: User):
    if create_user(DB, user.username, user.firstname, user.lastname, user.password):
        return {'msg': 'user created'}
    return {'msg': 'user already exists'}

### LOGIN (API) ###
@app.post('/login', response_model=Token)
async def login_api(form_data: OAuth2PasswordRequestForm = Depends()):
    if login_user(DB, form_data.username, form_data.password):
        token = jwt.encode(
            {'iat': datetime.datetime.utcnow(), 'sub': form_data.username},
            SECRET,
            algorithm='HS256'
        )
        return {'access_token': token, 'token_type': 'bearer'}
    raise login_exception

### UPDATE (API) ###
async def get_username(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, SECRET, algorithms='HS256')
        username: str = payload.get('sub')
        if username is None:
            raise login_exception
    except jwt.JWTError:
        raise login_exception
    return username

@app.put('/users')
async def update_api(update: UserUpdate, username: str = Depends(get_username)):
    if update_user(DB, username, update.firstname, update.lastname, update.password):
        return {'msg': 'user updated'}
    return {'msg': 'update failed'}

### REMOVE (API) ###
@app.delete('/users')
async def remove_api(username: str = Depends(get_username)):
    if remove_user(DB, username):
        return {'msg': 'user deleted'}
    return {'msg': 'user does not exist'}


### CREATE ###
def create_user(db, username, firstname, lastname, password):
    if username in db:
        return False
    db_set(db, username, firstname, lastname, password)
    return True

### LOGIN ###
def login_user(db, username, password):
    if username not in db:
        return False
    _first, _last, hashed_pw, salt = db[username]
    return hash_password(password, salt) == hashed_pw

### UPDATE ###
def update_user(db, username, firstname, lastname, password):
    if username not in db:
        return False
    db_set(db, username, firstname, lastname, password)
    return True

### REMOVE ###
def remove_user(db, username):
    if username not in db:
        return False
    del db[username]
    return True

### UTIL ###
def db_set(db, username, firstname, lastname, password):
    salt = new_salt()
    hashed_pw = hash_password(password, salt)
    db[username] = (firstname, lastname, hashed_pw, salt)

def new_salt():
    return os.urandom(N_SALT_RANDOM_BYTES)

def hash_password(password, salt): # FIXME: use argon2 for hashing? (included in passlib)
    derived_key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, N_HASH_ITERATIONS)
    return derived_key.hex()
